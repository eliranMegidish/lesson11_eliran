#include<iostream>
#include<conio.h>
#include<windows.h>
#include <thread>
#define SIZE 1000
using namespace std;

//COLORS LIST
//1: Blue
//2: Green
//3: Cyan
//4: Red
//5: Purple
//6: Yellow (Dark)
//7: Default white
//8: Gray/Grey
//9: Bright blue
//10: Brigth green
//11: Bright cyan
//12: Bright red
//13: Pink/Magenta
//14: Yellow
//15: Bright white
//Numbers after 15 include background colors

void Color(int color);
void printColor();
void getMaxNUmber(int begin, int end, int myArr[],int& maxNumber);
int multipleThreadsFindMaxNumber(int begin, int end,int myArr[],int N);

int main(void)
{
    int i = 0;

    
    for (i = 0; i < 15; i++)
    {
        Color(i+1);
        std::thread t(printColor);
        t.join();
    }
    system("pause");
    int arr[SIZE] = { 0 };
    for (i = 0; i < SIZE; i++)
    {
        arr[i]= rand() % 1000000;
    }
    cout << "Max number is:"<< multipleThreadsFindMaxNumber(0, 1000, arr, 5) << endl;;
   
    return 0;
}


void Color(int color)
{
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}


void printColor()
{
    int i = 0;
    for (i = 0; i < 10; i++)
    {
        cout << "Color" << endl;
    }
}


void getMaxNUmber(int begin, int end, int myArr[],int& maxNumber)
{
    int i = 0;
    maxNumber = 0;
    for (i = begin; i < end; i++)
    {
        if (myArr[i] > maxNumber)
        {
            maxNumber = myArr[i];
        }
    }
}


int multipleThreadsFindMaxNumber(int begin, int end, int myArr[], int N)
{
	int jump = (end - begin) / N;
    int endJump = 0;;
	int i = 0;
    int maxNum = 0;;
    int maxNumNow = 0;;

	for (i = 0; i < N; i++)
	{
		endJump = begin + jump;

		if (i == N - 1)
		{
			std::thread t(getMaxNUmber, ref(begin), ref(end), ref(myArr), ref(maxNum));
			t.join();
		}
		else
		{
			std::thread t(getMaxNUmber, ref(begin), ref(endJump), ref(myArr), ref(maxNum));
			t.join();
		}
		begin += jump;

        if (maxNum > maxNumNow)
        {
            maxNumNow = maxNum;
        }
	}
    return maxNumNow;
}
