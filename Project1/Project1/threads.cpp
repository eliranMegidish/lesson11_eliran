#include "threads.h"

void I_Love_Threads()
{
	cout << "I Love Threads" <<endl;
}


void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();
}


void printVector(std::vector<int> primes)
{
	int i = 0; 
	for (i = 0; i < primes.size(); i++)
	{
		cout <<primes[i]<< endl;
	}
}

/*
This function find all the Primes numbers between the range
*/
void getPrimes(int begin, int end, std::vector<int>& primes)
{
	int number = 0;
	int j = 0;
	bool isPrime = true;
	for (number = begin; number <= end; number++)
	{
		for (j = 2; j <= number / 2; ++j)
		{
			if (number % j == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			primes.push_back(number);
		}
		isPrime = true;
	}
}

/*
This function use thread to call function callGetPrimes and print 
The time it took to run from the beginning thread to the end.
*/
std::vector<int> callGetPrimes(int begin, int end)
{
	vector<int> Primes;

	clock_t time_req;
	time_req = clock();

	std::thread t1(getPrimes,ref(begin),ref(end),ref(Primes));
	t1.join();

	time_req = clock() - time_req;
	cout << "time took:" << (double)time_req / CLOCKS_PER_SEC << endl;

	return Primes;
}

/*
This function write to file all numbers primes 
between the number range.
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int number = 0;
	int j = 0;
	bool isPrime = true;

	for (number = begin; number <= end; number++)
	{
		for (j = 2; j <= number / 2; ++j)
		{
			if (number % j == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			file << number << "\n";
		}
		isPrime = true;
	}
}

/*
This function call to function writePrimesToFile by N threads
and print The total time it took to N threads run from the
beginning thread to the end.
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int jump = (end-begin)/N;
	int endJump = 0;;
	int i = 0;
	double sumTime = 0;

	ofstream myFile(filePath);
	if (!(myFile.is_open()))
	{
		cout << "Unable to open file";
	}

	clock_t time_req;
	for (i = 0; i < N; i++)
	{
		time_req = clock();
		endJump = begin + jump;
		if (i == N - 1)
		{
			std::thread t(writePrimesToFile, ref(begin), ref(end), ref(myFile));
			t.join();
		}
		else
		{
			std::thread t(writePrimesToFile, ref(begin), ref(endJump), ref(myFile));
			t.join();
		}
		begin += jump;
		time_req = clock() - time_req;
		sumTime += (double)time_req / CLOCKS_PER_SEC;
	}
	cout << "time took:" << sumTime << endl;
}
