#include "threads.h"
#include <iostream>
#include <fstream>


int main()
{

	call_I_Love_Threads();

	vector<int> primes3 = callGetPrimes(0, 1000);
	cout << "\n" << endl;
	primes3=callGetPrimes(0, 100000);
	cout << "\n" << endl;
	primes3 = callGetPrimes(0, 1000000);
	cout << "\n" << endl;

	callWritePrimesMultipleThreads(0, 1000, "eliran.txt", 4);
	callWritePrimesMultipleThreads(0, 100000, "eliran.txt", 4);
	callWritePrimesMultipleThreads(0, 1000000, "eliran.txt", 4);


	system("pause");
	return 0;
}